
package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;


/**Contains the JPanel that is the Canvas of the application where the disc and 
 * lines are drawn. Also contains inner class Coordinate Group that manages the
 * disc coordinates to draw a line between points.
 * Has methods to change from cartesian coordinates to polar coordinates and
 * the other way around. Contains pixel amount constants like disc Radius, Canvas
 * length and hoy many pixels is one unit of displacement. Draws the correspondent 
 * plane and the axis.
 * 
 * @author Nunila Davila
 * @author Roberto de Jesus 
 * @author Jesus Vega
 * @author Hector Rivera
 */

public class CanvasToDraw extends JPanel{
    
    private final int RADIUS = 8;
    private final int PIXELWIDTHUNIT=14;
    private final int HALFCANVASLENGTH=300;

    private float discX = 0;
    private float discY = 0;
    private float lastCoordinateX = 0;
    private float lastCoordinateY = 0;
    private boolean isCartesianPlane = true;
    private boolean isCartesianCoord = true;
    
    private List<CoordinateGroup> lineList = new ArrayList();
    
    
    /**Contains inner class Coordinate Group that manages the disc coordinates 
     * to draw a line between points.
    * @author Hector Rivera
    */
    public class CoordinateGroup{
        private float firstX;
        private float firstY;
        private float secondX;
        private float secondY;
        
        public CoordinateGroup(float firstX, float firstY, float secondX, float secondY){
            this.firstX = firstX;
            this.firstY = firstY;
            this.secondX = secondX;
            this.secondY = secondY;
        }
        
        public void setFirstX(float x){
            this.firstX = x;
        }
        public void setFirstY(float y){
            this.firstY = y;
        }
        public void setSecondX(float x){
            this.secondX = x;
        }
        public void setSecondY(float y){
            this.secondY = y;
        }
        public float getFirstX(){
            return this.firstX;
        }
        public float getFirstY(){
            return this.firstY;
        }
        public float getSecondX(){
            return this.secondX;
        }
        public float getSecondY(){
            return this.secondY;
        }
         
    }
    
    public CanvasToDraw() {
        
    }

    /**
    * Is called at the creation of the JPanel and each time repaint() is called
    * to paint the disc position, existing lines, chosen plane and the axis.
    * @author Nunila Davila
    * @author Roberto de Jesus 
    * @author Hector Rivera
    * @param g Graphics property of the JPanel
    */
    public void paintComponent(Graphics g){
        
         super.paintComponent(g);
        
        g.setColor(Color.BLACK);
        this.drawAxis(g);    
        
        if(!isCartesianPlane)  {
            drawPolarPlane(g);
        }
        else  { 
            drawCartesian(g);
        }
           
        this.drawNum(g);
        
        this.drawDisc(g);
        this.drawLines(g);
   
    }
    
    /**
    * Gets the user input for X/R and Y/Angle, converts polar coordinates to 
    * X and Y in order to obtain a usable coordinates for pixels and moves the disc
    * to that position.
    * @author Hector Rivera
    * @param x current x or r value in the input
    * @param y current y or angle value in the input
    */
    public void moveDisc(float x, float y) {      

        if(!this.isCartesianCoord){
             float temp1 = this.polarToX(x, y);
             float temp2 = this.polarToY(x, y);
             x = temp1;
             y = temp2;
        }
        discX = x;
        discY = y;
        repaint();
    }
    
    /**
    * Adds a line to the lineList so that it can be drawn in the paintComponent and 
    * sets the last Coordinates to the current disc position.
    * @author Hector Rivera
    * @param x current x or r coordinate of disc
    * @param y current y or angle coordinate of disc
    */
    public void addLine(float x, float y){
        if(!this.isCartesianCoord){
             float temp1 = this.polarToX(x, y);
             float temp2 = this.polarToY(x, y);
             x = temp1;
             y = temp2;
        }
        lineList.add(new CoordinateGroup(lastCoordinateX, lastCoordinateY, x, y));
        lastCoordinateX = x;
        lastCoordinateY = y;
        repaint();
    }
    
    /**
    * Draws the disc at the desired coordinates.
    * @author Hector Rivera
    * @param g Graphics property of the JPanel
    */
    public void drawDisc(Graphics g){
        g.setColor(Color.BLACK);           
        int x = (int) (discX*PIXELWIDTHUNIT + HALFCANVASLENGTH);
        int y = (int) (discY*-1*PIXELWIDTHUNIT + HALFCANVASLENGTH); //So that Y increases upwards

        g.fillOval(x-RADIUS,y-RADIUS,2*RADIUS,2*RADIUS);
        g.setColor(Color.WHITE);
        g.fillOval(x-RADIUS/2,y-RADIUS/2,RADIUS,RADIUS);
    }
    
    /**
    * Draws all the lines that exist in the lineList.
    * @author Hector Rivera
    * @param g Graphics property of the JPanel
    */
    public void drawLines(Graphics g){
        for(CoordinateGroup i: lineList){
            g.setColor(Color.BLACK);
            int firstX = (int)(i.getFirstX()*PIXELWIDTHUNIT + HALFCANVASLENGTH);
            int firstY = (int)(i.getFirstY()*-1*PIXELWIDTHUNIT + HALFCANVASLENGTH);
            int secondX = (int)(i.getSecondX()*PIXELWIDTHUNIT + HALFCANVASLENGTH);
            int secondY = (int)(i.getSecondY()*-1*PIXELWIDTHUNIT + HALFCANVASLENGTH);

            g.drawLine(firstX, firstY, secondX, secondY);
        }      
    }

    /**
    * Draws the numbers on the X and Y axis each 2 units.
    * @author Roberto de Jesus 
    * @param g Graphics property of the JPanel
    */
    public void drawNum(Graphics g){
        g.setColor(Color.DARK_GRAY);
        for(int i =HALFCANVASLENGTH,j = 0 ;i < HALFCANVASLENGTH*2 ; i += (PIXELWIDTHUNIT * 2) , j+=2){
            if(j == 0)
                continue;
            g.drawString(Integer.toString(j), i-4, 315);
            g.drawString(Integer.toString(-j), 302, i+4);
        }
        for(int i = HALFCANVASLENGTH ,j = 0;0 < i;i -= PIXELWIDTHUNIT * 2, j+=2){
            if(j == 0)
                continue;
            g.drawString(Integer.toString(-j), i-8, 315);
            g.drawString(Integer.toString(j), 302, i+4);
        }
    }
    
    /**
    * Draws the X and Y axis at the center of the panel.
    * @author Roberto de Jesus 
    * @param g Graphics property of the JPanel
    */
    public void drawAxis(Graphics g){
        g.setColor(Color.LIGHT_GRAY);
        // Y-axis
        g.drawRect(HALFCANVASLENGTH - 3, 0, 4, HALFCANVASLENGTH*2); // HALFCANVASLENGTH - 3 = 297
        g.fillRect(HALFCANVASLENGTH - 3, 0, 4, HALFCANVASLENGTH*2);
        // Upper-Arrow
        g.drawLine(290, 10, HALFCANVASLENGTH, 0); // left 
        g.drawLine(310, 10, HALFCANVASLENGTH, 0); // right 
        // Lower-Arrow
        g.drawLine(290, 590, HALFCANVASLENGTH, HALFCANVASLENGTH*2); // left
        g.drawLine(310, 590, HALFCANVASLENGTH, HALFCANVASLENGTH*2); // right
        // X-axis
        g.drawRect(0, HALFCANVASLENGTH - 3, HALFCANVASLENGTH*2, 4); //  HALFCANVASLENGTH - 3 = 297
        g.fillRect(0, HALFCANVASLENGTH - 3, HALFCANVASLENGTH*2, 4);
        // Left-Arrow
        g.drawLine(0, HALFCANVASLENGTH, 10, 290);
        g.drawLine(0, HALFCANVASLENGTH, 10, 310);
        // Right-Arrow
        g.drawLine(HALFCANVASLENGTH*2, HALFCANVASLENGTH, 590, 290);
        g.drawLine(HALFCANVASLENGTH*2, HALFCANVASLENGTH, 590, 310);
    }
    
    
    /**
    * Draws the cartesian plane at the center of the Axis and panel.
    * @author   Roberto de Jesus
    * @param g  Graphics property of the JPanel
    */
    public void drawCartesian(Graphics g){
        
        // Grid
        g.setColor(Color.LIGHT_GRAY);
        for(int i = 6;i<HALFCANVASLENGTH*2;i+=PIXELWIDTHUNIT){
            if(i == HALFCANVASLENGTH)
                continue;
            g.drawLine(i, 0,i , HALFCANVASLENGTH*2);
            g.drawLine(0, i,HALFCANVASLENGTH*2 , i);
        }
    }

    /**
    * Clears all existing lines in the plane and resets disc position to 0,0
    * @author Jesus Vega 
    */
    public void clearAndReset(){
        lineList.clear();
        this.resetDisc();
    }
    
    /**
    * Resets disc position to 0,0 and sets last coordinate to 0,0 to let the user start
    * over without erasing the path.
    * @author Jesus Vega
    */
    public void resetDisc(){
        this.lastCoordinateX = 0;
        this.lastCoordinateY = 0;
        this.discX = 0;
        this.discY = 0;
        repaint();
    }
    
    /**
    * Draws the polar plane at the center of the Axis and panel.
    * @author   Nunila Davila
    * @param g  Graphics property of the JPanel
    */
    public void drawPolarPlane(Graphics g) {     
        g.setColor(Color.GRAY);
        for(int i=1; i<=20;i++) {
            int circleDiameter = 2*i*PIXELWIDTHUNIT;
            g.drawOval(HALFCANVASLENGTH-circleDiameter/2, HALFCANVASLENGTH-circleDiameter/2 ,circleDiameter, circleDiameter);
        }
        for (int j=0; j<360;j+=30) {
            double rad = Math.toRadians(j);
            int x = (int) Math.round(HALFCANVASLENGTH*Math.cos(rad)) + HALFCANVASLENGTH;
            int y = (int) Math.round(HALFCANVASLENGTH*Math.sin(rad)) + HALFCANVASLENGTH; 
            g.drawLine(HALFCANVASLENGTH, HALFCANVASLENGTH, x, y);           
        }       
    }
    
    /**
    * Converts the X and Y coordinates to a radius in polar coordinates
    * @author   Jesus Vega
    * @param x  current X coordinate of disc to convert to radius
    * @param y  current Y coordinate of disc to convert to radius
    * @return float that represents the new radius in polar coordinates
    */
    public float cartesianToR(float x, float y){
        if(Float.isNaN(x) || Float.isNaN(y)) throw new java.lang.IllegalArgumentException(); 
        return (float)Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
    }
    
    /**
    * Converts the X and Y coordinates to an angle in polar coordinates
    * @author   Jesus Vega
    * @param x  current X coordinate of disc to convert to angle
    * @param y  current Y coordinate of disc to convert to angle
    * @return float that represents the new angle in polar coordinates
    */    
    public float cartesianToAngle(float x, float y){
        if(Float.isNaN(x) || Float.isNaN(y)) throw new java.lang.IllegalArgumentException(); //Prevent possible conversion NaN
        if (x==0) {
            if(y<0)         return 270;
            else if(y>0)    return 90;
            else            return 0;            
        }
        
        return (float)Math.toDegrees(Math.atan(y/x));      
    }
    
    /**
    * Converts the radius and angle coordinates to X in cartesian coordinates
    * @author   Jesus Vega
    * @param r  current radius coordinate of disc to convert to X coordinate
    * @param angle  current angle coordinate of disc to convert to X coordinate
    * @return float that represents the new X coordinate
    */
    public float polarToX(float r, float angle){
        if(Float.isNaN(r) || Float.isNaN(angle)) throw new java.lang.IllegalArgumentException(); 
        angle = (float) Math.toRadians(angle);
        return (float)(r*Math.cos(angle));
    }
    
    /**
    * Converts the radius and angle coordinates to Y in cartesian coordinates
    * @author   Jesus Vega
    * @param r  current radius coordinate of disc to convert to Y coordinate
    * @param angle  current angle coordinate of disc to convert to Y coordinate
    * @return float that represents the new Y coordinate
    */
    public float polarToY(float r, float angle){
        if(Float.isNaN(r) || Float.isNaN(angle)) throw new java.lang.IllegalArgumentException();         
        angle = (float) Math.toRadians(angle);
        return (float)(r*Math.sin(angle));
    }
    
    
    public void changeCoordinatesFormat(){
        this.isCartesianCoord = !this.isCartesianCoord;      
    }
    
    public boolean getIsCartesianCoors(){
        return this.isCartesianCoord;      
    }
    
    public boolean getIsCartesianPlane(){
        return this.isCartesianPlane;      
    }
    
    public void setPlane() {
        this.isCartesianPlane = !isCartesianPlane;
        repaint();
    }
 
}