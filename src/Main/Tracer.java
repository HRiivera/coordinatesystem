/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import GUI.CanvasToDraw;
import GUI.MainPanelWithCanvas;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Starts the Coordinate Placement application.
 * A user inputs a pair of coordinates so that a disc may move to the desired position 
 * and create a path of all its past locations.The user may choose the coordinate 
 * system and the background to either cartesian or polar. The user can also choose 
 * to force the disc to the origin or to erase the path and start over. 
 * @author Nunila Davila
 * @author Hector Rivera
 */

public class Tracer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createGUI(); 
            }
        });           
    }
    
    /**
    * Creates the GUI for the Coordinate placement application with the buttons,
    * inputs and canvas.
    * @author Nunila Davila
    * 
    */
    private static void createGUI() {
        JFrame frame = new JFrame("Tracer");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);      
            MainPanelWithCanvas gui = new MainPanelWithCanvas();
            CanvasToDraw canvas = new CanvasToDraw();
            gui.add(canvas);
            canvas.setSize(600, 600);
            canvas.setLocation(200, 0);
            canvas.setBackground(Color.white);
        
            gui.setCanvas(canvas);
            frame.setResizable(false);
            frame.add(gui);
            
            frame.pack();
            frame.setVisible(true);         
    }

    Tracer() {
        
    }
 

}
