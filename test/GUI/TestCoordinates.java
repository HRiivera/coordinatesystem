/*
 * Copyright (c) 2020 Jesus Vega.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jesus Vega Alejandro
 *    Hector Rivera
 */
package GUI;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jesus Vega Alejandro, Hector Rivera
 */
public class TestCoordinates {
    
    //Instances for 
    CanvasToDraw canvasT = new CanvasToDraw();
    float r, a, x, y;
  
     /**
    * Tests the Radius Calculation
    * @author Jesus Vega
    * @author Hector Rivera
    *
    */
   @Test 
   public void cartesianToRadiusTest(){
       //
       //Base Conversion Case
       r = canvasT.cartesianToR(2, 4);
       assertEquals(4.472f, r, 0.05);
       
       //Negative Input Radius Test
       r = canvasT.cartesianToR(-10, 2);
       assertTrue(r>0);
       assertEquals(10.20f, r, 0.05);
       
      //Negative handler
       r = canvasT.cartesianToR(-15, -10);
       assertTrue(r > 0);
       assertEquals(18.027f, r, 0.05);  
       
   }
   /**
    * Tests the Angle Calculation
    * @author Jesus Vega
    */
    @Test 
    public void cartesianToAngleTest(){
       //Base Case
       a = canvasT.cartesianToAngle(10, 15);
       assertEquals( 56.31f, a, 0.01); //Margin of error accepted settled to 0.01
       
         a = canvasT.cartesianToAngle(0.0678f, 16.87f);
         assertEquals(89.769f, a, 0.05);
       
       //Test division of parameters for the following cases
         a = canvasT.cartesianToAngle(0, 10); //Arithmetic Exception should be percieved by method
         assertEquals(true, Float.isFinite(a)); 
        
         a = canvasT.cartesianToAngle(0, -15);
         assertEquals(true, Float.isFinite(a)); //Arithmetic Exception should be percieved by method
         assertEquals(270f, a, 0);
        
       //Test division
         a = canvasT.cartesianToAngle(0, 0);
         assertEquals(false, Float.isInfinite(a)); //Arithmetic Exception shoul be percieved by method. Division by 0.
         assertEquals(false, Float.isNaN(a)); //Floating Point Standard Arithmetic exeption Handling; Division by 0 Not a Number
         assertEquals(0f, a, 0);
            
   }
    
     
    /**
    * Tests the X Coordinate Calculation
    * @author Jesus Vega
    */
    @Test 
     public void polarToXCoordinateTest(){
         
       //Base Case
       x  = canvasT.polarToX(18, 36);
       assertEquals(14.56f, x, 0.01);
    
       
       //Test Negative
       x = canvasT.polarToX(21, -136.78f);
       assertFalse(x>0);
       assertEquals(-15.30f, x, 0.01); //Passable pressition error to 0.1.
         
   }
       /**
    * Tests the Y Coordinate Calculation
    * @author Jesus Vega
    */
      @Test 
      public void polarToYCoordinateTest(){
          
       //Base Conversion
       y  = canvasT.polarToY(2, 68);
       assertEquals(1.85f, y, 0.02);
       
       y = canvasT.polarToY(3.89f, 56.87f);
       assertEquals(3.25f, y, 0.05);
       
       //Test Negative Angle
       y = canvasT.polarToY(3, -68);
       assertFalse(y>0);
       
   }
      
      /**
    * Verify Not a Number Handlers
    * @author Jesus Vega
    */
    @Test(expected=IllegalArgumentException.class) //Author Jesus G. Vega
    public void ArithmeticErrArg(){ 
        //Not a Number Floating Execption encountered when Arithmetic Error is caugh with Tangent or Sine
        a = canvasT.cartesianToAngle(3, Float.NaN); 
        System.out.println(a);
    }
    
    
      
   
}
